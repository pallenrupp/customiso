#!/bin/bash
#
# This script helps build customized RHEL/CentOS .iso boot images from 
# DVD media.  The program figures out and runs the correct commands needed
# which vary slightly depending on the rhel/cent version.
#
# Usage:
#   pgm -d <distro dir> [-c <customization dir>] [-o <iso file>]
#
# Options:
#    -d is required and is the path to your distro directory. 
#    -w is required and is the path to your work directory. 
#    -c is optional, and points to an alternate directory containing
#       any customizations you want to include in the media before creating
#       the iso.
#    -o is optional, and specifies the output .iso file name.
#    -k (optional) "keep" the work directory when done.
#
# Notes: 
#    The program should be supplied with a valid RHEL/CENT distribution
#    directory, either as a read-only mount point to a DVD image, or a
#    writable file-system copy of a mounted DVD image.  This is done using
#    the required "-d" option.  The program looks inside the distro directory
#    for certain rpm's to determine the major RHEL/CENT version (currently 6 or 7),
#    and uses this information to alter the command used to generate the iso.
#    If a version cannot be determined, the program exits with an error. 
#    By default, the name of the generated .iso image is named after the distro 
#    directory with a .iso suffix and deposited in the current directory
#    of the distro. Alternatively, you may specify a different .iso name
#    using the "-o" option.
#    Finally, you may customize the generated .iso by specifying a directory
#    structure containing customizations using the "-c" option.  The program
#    will copy the structure into your distro directory prior to generating 
#    the .iso image.  This means both the directory structure and file names
#    contained in the customization directory should *exactly* match the distro
#    you are building.  Because your customizations are copied into the distro
#    directory, it must be writable.  Usually, when making customizations, you
#    will create a writable disk copy of a mounted distro DVD.
#

PGM=$(basename $0)
export PATH=$PATH:$(readlink -f $(dirname $0))

CONFDIR=""
DISTDIR=""
WORKDIR=""
ISOFILE=""
KEEP=0

usage() {
  echo "Usage: $(basename $0) -d /path/to/distro [-c /path/to/config/dir] [-o /path/to/output.iso]"
  echo "Where"
  echo "  -d (required) is a linux distribution directory - usually a mounted .iso"
  echo "  -c (optional) directory structure of configuration files"
  echo "  -w (required) is a work directory - usually a mounted .iso"
  echo "  -o (optional) - default .iso name is <DISTDIR>.iso"
  echo "  -k (optional)  keep the work directory when finished"
  echo ""
  return 1
}

log() {
   # "$@" contains message string
   # log format:
   #      Timestamp           Producer  Result
   #      YYYY.MM.DD.HH:MM:SS <msg>
   #
   # TIMESTAMP=`date  "+%Y.%m.%d.%T(%Z)"`
   # echo "$PGM: $TIMESTAMP $@"
   echo "$@"
}


verify() { 
  while getopts c:w:d:o:k OPTION "$@"
  do
     case ${OPTION} in
     c) CONFDIR=$OPTARG
        ;;

     d) DISTDIR=$OPTARG
        ;;

     w) WORKDIR=$OPTARG 
        ;;

     o) ISOFILE=$OPTARG
        ;;

     k) KEEP=1
        ;;

     \?) log "Invalid option: -${OPTARG}" 
         ;;
     *) usage
        exit 2;;
     esac
  done
  #
  # Sanity checks on parameters...
  #

  if [[ $DISTDIR == "" ]]; then 
     log "Error: -d <distro dir> missing, exiting."
     usage
     exit -1
  fi 
  if [[ ! -d $DISTDIR ]]; then
     log "Error: $DISTDIR is not a valid directory, exiting..."
     usage
     exit -1
  fi
  if [[ $WORKDIR == "" ]]; then 
     log "Error: -w <work dir> missing, exiting."
     usage
     exit -1
  fi 
  if [[ ! -d $WORKDIR ]]; then
     log "Error: $WORKDIR is not a valid directory, exiting..."
     usage
     exit -1
  fi

  if [[ $CONFDIR != "" ]]; then 
     if [[ ! -d $CONFDIR ]]; then
        log "Error: $CONFDIR is not a valid directory, exiting..."
        usage
        exit -1
     fi
  fi

  if [[ "$ISOFILE" == "" ]]; then
     # ISO file defaults to <DISTDIR>.iso
     FILE=$(basename ${DISTDIR})
     DIR=$(dirname ${DISTDIR})
     ISOFILE="${FILE}.iso"
  else 
     FILE=$(basename $ISOFILE)
     DIR=$(dirname $ISOFILE)
     if [[ ! -d $DIR ]]; then
        log "Error: \"-o $OPTARG\"  has an invalid directory."
        exit -1
     fi
  fi

  DISTDIR=$(readlink -f $DISTDIR)
  CONFDIR=$(readlink -f $CONFDIR)
  WORKDIR=$(readlink -f $WORKDIR)
  ISOFILE=$(readlink -f $ISOFILE)
}

doit() {
  log "INFO: Buildng customized RHEL/CENT .iso." $ISO from 
  log "INFO: Distribution  =$DISTDIR"
  log "INFO: Configuration =$CONFDIR"
  log "INFO: Work directory=$WORKDIR"
  log "INFO: ISO file      =$ISOFILE"
  log "INFO: Keep          =$KEEP"

  #
  # step 1.  Get the 'mkisofs' command from the distro directory
  #          (If we cannot, we have to stop.)

  CMD=""
  log "INFO: Get distro LABEL..."
  LABEL=$(get-label $DISTDIR | grep '^CLEANLABEL=' | awk -F= '{print $2}')
  echo "LABEL=$LABEL, RC=$?"

  if [[ $? -ne 0 ]]; then
     log "WARN: Cannot find a LABEL for this distribution!!"
  fi
  log "INFO: label found --> $LABEL"

  #
  TMPDIR="$WORKDIR/tmp.$(date  "+%Y.%m.%d.%T")" 
  
  log "INFO: Copy work/config directories into temp build dir $TMPDIR" 
  if mkdir -p $TMPDIR; then
     log "INFO: tar-up distribution directory to temp build area.."
     cd $DISTDIR
     tar -cf  - ./* | (cd $TMPDIR; tar -xf - )
     if [[ $? -eq 0 ]]; then
        log "INFO: Successful!"
        log "INFO: tar-up config directory to temp build area.."
        cd $CONFDIR
        tar -cf  - ./* | (cd $TMPDIR; tar -xf -)
        if [[ $? -eq 0 ]]; then
           log "INFO: Successful!"
        else
           log "ERROR: Unsuccessful! Exiting."
           exit 1
        fi
     else
        log "ERROR: Could not change into $CONFDIR"
        exit 1
     fi
  else
     log "ERROR: could not create tmp directory $TMPDIR!"
     exit 1
  fi

  log "INFO: distro/config copied OK..." 
  log "INFO: create build command..."

  #   
  # build mkisofs command...
  #
  MKISOFS_CMD="mkisofs -U -A ${LABEL} -V ${LABEL} -volset ${LABEL} -J -joliet-long -r -v -T  -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -eltorito-alt-boot -e images/efiboot.img -no-emul-boot -o '${ISOFILE}' '$TMPDIR'"
  #
  # Run mkisofs... 
  #
  log "INFO: Running CMD=$CMD"
  set -x
  eval "$MKISOFS_CMD"
  set +x
  log "INFO: Done. RC=$?"
  log "INFO: Saving commands..."
  #  
  # As a convenience, save the build/mkisofs commands to run manually if desired 
  #
  BUILD_CMD=$(echo "cd $(pwd)";\tr '\0' ' ' < /proc/$$/cmdline)
  for i in  $TMPDIR $WORKDIR  $CONFDIR
  do
     echo "$MKISOFS_CMD" > $i/mkisofs.cmd
     echo "$BUILD_CMD" > $i/$PGM.cmd
  done

  if [[ $KEEP -ne 1 ]]; then
     echo "INFO: Removing temporary directory $TMPDIR..."
     rm -rf $TMPDIR
     echo "INFO: Complete, RC=$?"
  fi
  echo "INFO: Done on $(date)" 
}
verify "$@"
doit
