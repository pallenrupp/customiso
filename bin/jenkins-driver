#!/bin/bash
#
PGM=$(basename $0)
export PATH=$PATH:$(readlink -f $(dirname $0))

SOURCE_URL=http://192.168.117.10/opt/downloads/iso/
TARGET_URL=http://192.168.117.10/cgi-bin/upload.py
UPLOAD_CMD="curl  -v -F 'file=@build-iso.cmd'  http://192.168.117.10/cgi-bin/upload.py"

CONFDIR=""
WORKDIR=""

usage() {
  echo "Usage: $(basename $0) -w <work dir> -c <config dir> [<step>...]"
  echo "Where"
  echo "  -c (required) custom iso configurarion directory (should contain isobuild.env)"
  echo "  -w (required) custom iso work/scratch directory."
  echo "  <step> is a step to run - must be 'prep', 'build', or 'post'"
  echo "  if <step> is omitted, execute the 'prep', 'build', 'post' steps in order." 
  echo "  if <step> is specified, execute the steps in specified order."
  echo ""
  echo " customiso.env should contain the following bash variables:"
  echo "    ENABLE=[0|1] Flag indicating to continue (1) or not (0)"
  echo "    SOURCE_ISO=<name of source iso file>"
  echo "    CUSTOM_ISO=<name of custom iso file>"
  echo " e.g," 
  echo "    ENABLE=1" 
  echo "    SOURCE_ISO=CentOS-7-x86_64-DVD-1908.iso"
  echo "    CUSTOM_ISO=petes-centos7-custom.iso"
  echo ""
  exit 1
}

log() {
   # "$@" contains message string
   # log format:
   #      Timestamp           Producer  Result
   #      YYYY.MM.DD.HH:MM:SS <msg>
   #
   # TIMESTAMP=`date  "+%Y.%m.%d.%T(%Z)"`
   # echo "$PGM: $TIMESTAMP $@"
   echo "$@"
}

validate_md5() {
  #
  # simple md5sum output validator.
  # Input is a single string, containing two words, the first is the md5 checksum
  # and the second is the name of a file. We verify if it has 1 line, 2 words, and
  # the first is 32-characters long.  If it meets these requirements,it returns "0"
  # success (in bash).
  #
  echo "Inside function: ${FUNCNAME[0]}"
  rc=1
  read -a a <<< $@
  read -a b <<< $(echo $md5|wc)  
  # Ensure md5 string has 1 line, and the first word is 32 bytes." 
  if [[ ${#a[0]} == 32 && ${b[0]} == 1 ]]; then
     rc=0 
  fi
  return $rc
}


do_pre() {
   echo "Inside function: ${FUNCNAME[0]}"
   log "INFO: $PGM Variables:"
   log "INFO:    WORKDIR=$WORKDIR"
   log "INFO:    CACHEDIR=$CACHEDIR"
   log "INFO:    BUILDDIR=$BUILDDIR"
   log "INFO: Job variables:"
   log "INFO:    ENABLE=$ENABLE"
   log "INFO:    CACHE=$CACHE"
   log "INFO:    SOURCE_ISO=$SOURCE_ISO"
   log "INFO:    TARGET_ISO=$TARGET_ISO"
   log "INFO:    CONFDIR=$CONFDIR"
   log ""

   #SOURCE_URL=http://192.168.117.10/opt/downloads/iso/
   #TARGET_URL=http://192.168.117.10/cgi-bin/upload.py
   #UPLOAD_CMD="curl  -v -F 'file=@build-iso.cmd'  http://192.168.117.10/cgi-bin/upload.py"

   #
   # Download and validate the source iso's MD5 file (required)
   # (to be used later to look up a previously cached iso)
   #
   log "INFO: Download and validate MD5 checksum for $SOURCE_ISO"
   source_md5=$(curl -L --fail ${SOURCE_URL}/$SOURCE_ISO.md5 2>/dev/null | awk '{print $1}') 
   log "INFO: Checksum file ${SOURCE_URL}/${SOURCE_ISO}.md5 downloaded ok."
   if validate_md5 $source_md5; then
      log "INFO: checksum  file looks ok, continuing..."
   else
      log "ERROR: checksum file looks bad, exiting..."
      exit 1
   fi

   # 
   # Check if existing cached directories have same md5...
   # if so, no need to download .iso, and use cached directory
   #
  
   CACHE_ISO_FILE=""
   for i in $(find $CACHEDIR -maxdepth 1 -type f -name '*.iso' 2> /dev/null)
   do
      log "INFO: Checking cache entry $i" 
      md5_file=${i}.md5
      if [[ -f $md5_file  ]]; then
         cache_md5=$(cat $md5_file | awk '{print $1}')
         if [[ "$source_md5" == "$cache_md5" ]]; then
             CACHE_ISO_FILE=$i
         fi
      else
         log "WARN: Cache entry $i, missing .md5 file!"
      fi
   done

   if [[ $CACHE_ISO_FILE == "" ]]; then
      log "INFO: No cache entry found, create new one."
      # No cache md5 matched, assuming no cache entry...
      # Read the SOURCE_ISO into the CACHEDIR, then create an md5 file
      CACHE_ISO_FILE=$CACHEDIR/$SOURCE_ISO
      log "INFO: Downloading ${SOURCE_URL}/$SOURCE_ISO into $CACHE_ISO_FILE" 
      curl -L --fail ${SOURCE_URL}/$SOURCE_ISO --output $CACHE_ISO_FILE
      if [[ $? -eq 22 ]]; then
         log "ERROR: Download of iso $SOURCE_ISO failed."
         exit 1
      fi
      # Create md5 file
      echo "$source_md5" > ${CACHE_ISO_FILE}.md5
   else
      log "INFO:  CACHE entry found! Using $CACHE_ISO_FILE as iso source."
   fi 
   # because the iso file is mounted dynamically and will not survive a reboot
   # I always (re)mount it to ensure it exists. 
   CACHE_ISO_DIR=${CACHE_ISO_FILE%%.iso}
   mkdir -p $CACHE_ISO_DIR
   mount -t iso9660 $CACHE_ISO_FILE $CACHE_ISO_DIR
   export CACHE_ISO_FILE CACHE_ISO_DIR
   return 0
}

do_build() {
   echo "Inside function: ${FUNCNAME[0]}"
   log "INFO: $PGM Variables:"
   log "INFO:    WORKDIR=$WORKDIR"
   log "INFO:    CACHEDIR=$CACHEDIR"
   log "INFO:    BUILDDIR=$BUILDDIR"
   log "INFO: Job variables:"
   log "INFO:    ENABLE=$ENABLE"
   log "INFO:    CACHE=$CACHE"
   log "INFO:    SOURCE_ISO=$SOURCE_ISO"
   log "INFO:    TARGET_ISO=$TARGET_ISO"
   log "INFO:    CONFDIR=$CONFDIR"
   log "INFO:    CACHE_ISO_DIR=$CACHE_ISO_DIR"
   log "INFO:    CACHE_ISO_FILE=$CACHE_ISO_FILE"
   log ""
   #
   # Build the .iso
   #
   TIMESTAMP=$(date '+%F_%T')
   MYBUILDDIR=$BUILDDIR/$TIMESTAMP
   MYWORKDIR=$MYBUILDDIR/work
   mkdir -p $MYBUILDDIR
   mkdir -p $MYWORKDIR
   BUILD_TARGET_ISO=$(basename $TARGET_ISO)

   set -x
   if build-iso -d "$CACHE_ISO_DIR" -c "${CONFDIR}" -w "${MYWORKDIR}" -o "${MYBUILDDIR}/${BUILD_TARGET_ISO}"; then
      echo "Build DONE."
   else
      echo "Build FAILED."
   fi
}
do_post() {
   #echo "Inside function: ${FUNCNAME[0]}"
   log "INFO: Invoked ${FUNCNAME[0]} CONFDIR=$CONFDIR WORKDIR=$WORKDIR"
   #
   # Push new iso to final resting place
   #UPLOAD_CMD="curl  -v -F 'file=@build-iso.cmd'  http://192.168.117.10/cgi-bin/upload.py"
   log "INFO: MYBUILDDIR=$MYBUILDDIR"
   log "INFO: MYWORKDIR=$MYWORKDIR"
   log "INFO: BUILD_TARGET_ISO=$BUILD_TARGET_ISO"
   if cd $MYBUILDDIR; then
      curl  -v -F "file=@$BUILD_TARGET_ISO"  http://192.168.117.10/cgi-bin/upload.py
   fi

}

verify() { 
   #echo "Inside function: ${FUNCNAME[0]}"
   while getopts c:w: OPTION "$@"
   do
     case ${OPTION} in
     c)  
        CONFDIR=$OPTARG
        ;;
     w)
        WORKDIR=$OPTARG 
        ;;
     :) 
        log "Error: -$OPTARG requires an argument."
        usage
        ;;
     \?)
        log "Error: Invalid option -${OPTARG}" 
        usage
        ;;
     *) 
        usage
        ;;
     esac
  done
  #
  # Sanity checks on parameters...
  #
  CONFDIR=$(readlink -f $CONFDIR)
  WORKDIR=$(readlink -f $WORKDIR)

  if [[ $WORKDIR == "" ]]; then
     echo "Error: -w <workdir> missing, required."
     usage
     exit 1
  fi
  if [[ $CONFDIR == "" ]]; then
     echo "Error: -c <confdir> missing, required."
     usage
     exit 1
  fi

  if [[ ! -e $CONFDIR  && ! -d $CONFDIR ]]; then
     echo "Error: config directory $CONFDIR does not exist or is not a valid directory." 
     exit 1
  fi
  if [[ ! -d $WORKDIR ]]; then
     echo "Error: work directory $WORKDIR does not exist or is not a valid directory." 
     exit 1
  fi

}
doit() { 
   echo "Inside function: ${FUNCNAME[0]}"
   #
   # Get environment file...
   ENVFILE="$CONFDIR/$PGM.env"
   log "INFO: Read environment file $ENVFILE..." 
   if [[ -f "$ENVFILE" ]]; then
      . "$ENVFILE"
      log "INFO:   file exists."
      log "INFO:   ENABLE=$ENABLE"
      log "INFO:   CACHE=$CACHE"
      log "INFO:   SOURCE_ISO=$SOURCE_ISO"
      log "INFO:   TARGET_ISO=$TARGET_ISO"
   else
      log "Error:  file does not exist; required.  Exiting."
      exit 1
   fi

   #
   # Exit if not enabled
   #
   if [[ $ENABLE == "" ]]; then
      log "INFO: Not enabled, skipping."
      exit 1
   fi

   #
   # If enabled, continue...
   #

   WORKDIR=${WORKDIR}/customiso
   CACHEDIR=${WORKDIR}/cache
   BUILDDIR=${WORKDIR}/build

   mkdir -p $WORKDIR
   mkdir -p $CACHEDIR
   mkdir -p $BUILDDIR

   do_pre
   do_build
   do_post
}

log "INFO: Starting $PGM on $(date)"
verify $@
doit $@
