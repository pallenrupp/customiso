#!/bin/bash
#
# This script tries to guess the linux distribution type, and version
# based on the presence of certain 'release' rpm names.
#
# Notes: 
#   Very limited - currently useful for redhat, centos, and fedora 
#   distros that include 'release' rpms (used to establish the OS release)
#   Returns 0 if known, 1 if unknown returns rc=1 "unknown".
#   emits shell, json and yaml output, when using the -s|--style option
#

export PATH=$PATH:$(readlink -f $(dirname $0))
PGM=$(basename $0)

usage() {
  echo "Usage: $PGM </path/to/linux/distro>"
  echo 'Note: possible DISTRO values are: "redhat", "fedora", "centos"'
  exit 1
}

if [[ $# -eq 0 ]]; then
   echo "Error: missing options."
   usage
fi

DISTDIR=$1

if  ! test -d $DISTDIR; then
   echo "Error: $DISTDIR is not found or not a directory."
   exit 1
fi
  
rc=1

BLA=$(get-distro $DISTDIR)
if [[ $? -ne 0 ]]; then
   echo "Error: $PGM cannot get LABEL, unknown distro."
   exit 1
else
   eval "$BLA"
   echo "DISTRO=$DISTRO"
   echo "MAJOR=$MAJOR"
   echo "MINOR=$MINOR"
fi   

# Figure out the label

if [[ $DISTRO == "redhat"  || $DISTRO == "centos" ]]; then
   if [[ $MAJOR -eq 6 ]]; then
      LABEL="disks"
   elif [[ $MAJOR -gt 6 ]]; then
      LABEL=$(grep "hd\:LABEL" $(find $DISTDIR -name isolinux.cfg  | head -1) | head -1 | awk -FLABEL= '{print $2}' | awk '{print $1}')
   fi
   LABEL=$(python -c "print \"$LABEL\"")
elif [[ $DISTRO == "fedora" ]]; then
   LABEL="disks"
else
   LABEL=""
fi
    
echo "LABEL=$LABEL"
CLEANLABEL=$(echo ${LABEL} | sed 's/\(.\)/\\\1/g')
echo "CLEANLABEL=$CLEANLABEL"
