Creating custom kickstart DVD (.iso) media. 

Procedure to create a customized RHEL installation bootable .iso, that when booted 
presents the user  with a customized Anaconda menu to select customized kickstart
profiles.  You may add as many menu items and custom kickstart profiles as you wish.

Procedure:

1. Mount a stock RHEL/CentOS distribution .iso, and copy it to a separate media directory 
   e.g,
   # mount -t iso9660 -o loop /opt/depot/rhel/iso/rhel-server-7.3-x86_64-dvd.iso /mnt
   # cp -pr /mnt /opt/depot/rhel/iso/rhel73custom
   Where  /opt/depot/rhel/iso/rhel73custom is your new media directory.

2. Place your customized kickstart profiles in the top-level media directory
   e.g,
	/opt/depot/rhel/iso/rhel-server-7.3-x86_64-dvd/rhel7-bios-cloud-ks.cfg

3. Update the Anaconda menu's to point to your custom kickstart profiles.
   NOTE WELL!!!  Anaconda uses different menu systems, depending on if you boot
                 via uEFI or BIOS! Red Hat ad CentOS support both, and so shall
                 we. 

   for uEFI boot of DVD...
   RHEL 6.X:
      # cd <media>/EFI/BOOT
      # cp BOOTX64.conf BOOTX64.conf.orig
      # vim BOOTX64.conf
      Then, add your custom menu items...
      E.g, (adding 2 new custom menu items) 
      ----------------- clip here ---------------------
       title CUSTOM KICK RHEL 6.8  "CLOUD" (uEFI size=55G part=bios-b1H,r30L,s8L,t1M,p4L,o8L)
       kernel /images/pxeboot/vmlinuz ks=cdrom:/rhel6-uefi-cloud-ks.cfg
       initrd /images/pxeboot/initrd.img

       title CUSTOM KICK RHEL 6.8  "TRAD"  (uEFI size=80G part=bios-b1H,r25L,s8L,v5L,d16L,t4L,h8L,p4L,o8L)
       kernel /images/pxeboot/vmlinuz ks=cdrom:/rhel6-uefi-cloud-ks.cfg
       initrd /images/pxeboot/initrd.img
      ----------------- clip here ---------------------
      
   RHEL 7.x:

      # cd <media>/EFI/BOOT
      # cp grub.cfg grub.cfg.orig
      # vim grub.cfg
      Then, add your custom menu items....

      E.g, (adding 2 new custom menu items) 
      ----------------- clip here ---------------------
      menuentry 'CUSTOM KICK 1'  --class fedora --class gnu-linux --class gnu --class os {
      linuxefi /images/pxeboot/vmlinuz inst.stage2=hd:LABEL=RHE-7.3\x20Server.x86_64
      inst.ks=cdrom:/customkick1-ks.cfg
      initrdefi /images/pxeboot/initrd.img
      }
      menuentry 'CUSTOM KICK 2'  --class fedora --class gnu-linux --class gnu --class os {
      linuxefi /images/pxeboot/vmlinuz inst.stage2=hd:LABEL=RHE-7.3\x20Server.x86_64
      inst.ks=cdrom:/customkick2-ks.cfg
      initrdefi /images/pxeboot/initrd.img
      }
      ----------------- clip here ---------------------
   
   b. for BIOS boot of DVD...

      RHEL 6.X AND RHEL 7.X
      # cd <media>/isolinux
      # cp isolinux.cfg isolinux.cfg.orig
      # vim isolinux.cfg
      Then, add your custom menu items....
      E.g, (adding 2 new custom menu items) 
      ----------------- clip here ---------------------
      label linux
      menu label ^CUSTOM KICK 1
      kernel vmlinuz
      append initrd=initrd.img inst.stage2=hd:LABEL=RHEL-7.3\x20Server.x86_64 inst.ks=cdrom:/customkick1-ks.cfg
      label linux
      menu label ^CUSTOM KICK 2
      kernel vmlinuz
      append initrd=initrd.img inst.stage2=hd:LABEL=RHEL-7.3\x20Server.x86_64 inst.ks=cdrom:/customkick2-ks.cfg

4. Find the media "LABEL".. 
   Anaconda finds installation media using a partition label, you have to set it to specific
   wording, depending on the version.
   
   For  Version 7.X 
      a.  Edit or view  <media>/isolinux/isolinux.cfg
          Search for "hd:LABEL=".
          The "LABEL" is the stuff to the right of the "=" sign...
          E.g,
            "RHEL-7.3\x20Server.x86_64" 
            "CentOS\x207\x20x86_64"
          or similar.

      b. The label may contain hexadecimal representations of display characters, 
         for example "\x20" to represent an ascii " " (blank).
         You need to convert the hex characters to real blanks as follows:

         E.g,
         Convert "RHEL-7.3\x20Server.x86_64" to "RHEL-7.3 Server.x86_64"
         Convert "CentOS\x207\x20x86_64" to "CentOS 7 x86_64"
 
         Your "LABEL" is then "RHEL-7.3 Server.x86_64" or "CentOS 7 x86_64" (or similar)
         Save this for the next step...

    For Version 6.X
         The "LABEL" is always the word "disks".

5. Finally, create the .iso image

   In the command line below, update the '<LABEL>' with the label found in the previous step.
   And, update the -o option, this is your custom .iso image. 
   

   # cd <media directory>
   # mkisofs -U -A '<LABEL>' -V '<LABEL>' -volset  '<LABEL>' -J -joliet-long -r -v -T  -o ../rhel73custom.iso -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -eltorito-alt-boot -e images/efiboot.img -no-emul-boot . 


Done

Appendix A: Using Overlay mounts

    As of Linux kernel version 3.18 (3.10 CentOS/RHEL), the OS now supports overlay/union
    filesystems, as made popular by Docker.  You can use them too.
    In our case for building custom dvd isos, we place our iso configuration changes 
    in a separate directory, then overlay them over a DVD image.  When this occurs, you
    can create an iso from a union of the (customized) configuration files, with a
    stock DVD without having to copy the files to the DVD directory.
    In this way, you don't have to copy your files into the iso image, nor create 
    a full read-write copy of the iso.  Saves both disk-space and time to copy a full dvd.

    See:  https://www.kernel.org/doc/Documentation/filesystems/overlayfs.txt

    I used the following command
    
    #  mount -t overlay overlay -o lowerdir=CentOS-7-x86_64-DVD-1908,upperdir=rhel7custom_config,workdir=work CentOS-7-x86_64-DVD-1908_overlay

    where...
    1. CentOS-7-x86_64-DVD-1908 is a copy of the dvd image
    2. rhel7custom_config       is a customized directory structure
    3. workdir                  an empty work directory (hold COW updates to the upperdir)
    4. CentOS-7-x86_64-DVD-1908_overlay is the final mount (union) of the upper and lower directories


    NOTE WELL :-)
    A read-write mount is created when you use the 'upperdir=' and 'workdir=' directives, and 'upperdir=' becomes the writeable dir.

    You can create a Union mount of only lower directories, and in this case the entire mount is read only
    The following example creates a read-only mount composed of numerous merged directories.
    # mount -t overlay overlay -o lowerdir=this:that:and:another mnt


Appendixtom_config: Easy way to setup networks on bare-metal installs...

    The following procedure allows you to setup permanently configured
    interfaces (preserved across boot) in /etc/sysconfig/network-scripts/ifcfg-<connection name>

    Setup a DHCP interface (e.g, 'enp0s3') 
       # systemctl enable NetworkManager
       # systemctl start NetworkManager
       # nmcli con add con-name enp0s3 ifname enp0s3 type Ethernet
       # ifup enp0s3

    Setup a static interface (e.g, 'enp0s3')
       # systemctl enable NetworkManager
       # systemctl start NetworkManager
       # nmcli con add con-name enp0s8 ifname enp0s3 type Ethernet  ip4 192.168.0.20/24
       # ifup enp0s3

